//console.log("Test");

// JSON - Javascript Object Notation--------------------------------------
/* Syntax
{
	"propertyA" : "valueA",
	"propertyB" : "valueB"
}*/
// JSON Object--------------------------------------------------------------
/*{
	"city" : "Quezon City",
	"province" : "Metro Manila",
	"country" : "Philippines"
}
*/
// JSON Array--------------------------------------------------------------

/*"cities" : [
	// Array 1
	{ 	
	"city" : "Quezon City",
	"province" : "Metro Manila",
	"country" : "Philippines"
	},

	// Array 2
	{ 	
	"city" : "Manila City",
	"province" : "Metro Manila",
	"country" : "Philippines"
	},

	// Array 3
	{ 	
	"city" : "Makati City",
	"province" : "Metro Manila",
	"country" : "Philippines"
	}
]
*/
// JSON Methods--------------------------------------------------------------
// contains methods for parsing and converting data into stringified JSON
let batchesArr = [
	{
		batchName: "Batch 217",
		schedule: "Full-Time"	
	},
	{
		batchName: "Batch 218",
		schedule: "Part-Time"	
	}
]

// STRINGIFY convert method for JS Objects into string
console.log("Result from stringify method:");
console.log(JSON.stringify(batchesArr)); //converts normal JS Objects into string (json objects)

let data = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city: "Manila",
		country: "Philippines"
	}
});

console.log(data);

// Using STRINGIFY Method with Variables
let firstName = prompt("What is your first name:");
let lastName = prompt("What is your last name:");
let age = prompt("What is your age:");
let address = {
		city: prompt("Which city do you live in:"),
		country: prompt("Which country does your city address belongs to:")
};


let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
});

console.log(otherData);

// CONVERT JSON Object into Javascript Objects --------------------------------------------
// PARSE METHOD (using backticks)
let batchesJSON = `[
	{
		"batchName" : "Batch 217",
		"schedule" : "Full-Time"
	},

	{
		"batchName" : "Batch 218",
		"schedule" : "Part-Time"
	}
]`;

console.log("Result from parse method:");
console.log(JSON.parse(batchesJSON));

// Example 2 of PARSING (integer 31 is okay to not be inside double quotes)
let stringifiedObject = `{
	"name" : "John",
	"age" : 31,
	"address" : {
		"city" : "Manila",
		"country" : "Philippines"
	}
}`;

console.log("Result#2 from parse method:");
console.log(JSON.parse(stringifiedObject));